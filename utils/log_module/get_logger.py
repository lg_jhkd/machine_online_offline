import logging
import logging.config
import os
import sys
import traceback
import time
import yaml
# from config.inc import BASE_DIR
from weixin_tools import WeixinTools, WeixinToolsException


CURRENT_DIRECTORY = os.path.dirname(os.path.realpath(__file__))

# 初始化日志
time_str = time.strftime("%Y-%m-%d")
logger_config = yaml.load(open(os.path.join(CURRENT_DIRECTORY, 'log_config.yaml')))
for key in logger_config['handlers']:
    if 'filename' in logger_config['handlers'][key]:
        logger_config['handlers'][key]['filename'] = "logs/" + logger_config['handlers'][key]['filename'] + time_str + ".log"
logging.config.dictConfig(config=logger_config)


def get_online_logger():
    return logging.getLogger('onlineLogger')

def get_offline_logger():
    return logging.getLogger('offlineLogger')

def get_business_logger():
    return logging.getLogger('bussinessLogger')


class DecoratorLogger(object):
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        logger = get_business_logger()
        try:
            return self.func(*args, **kwargs)
        except Exception:
            logger.error(traceback.format_exc())


# 未捕获的异常
def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logger = logging.getLogger('systemLogger')
    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    if not issubclass(exc_type, WeixinToolsException):
        obj = WeixinTools()
        obj.send_message("online or offline sku in odoo Uncaught exception!\n请查看business.log获取详细信息!", to_users=[1023])


sys.excepthook = handle_exception
