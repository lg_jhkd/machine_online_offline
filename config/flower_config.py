import os

timezone = 'Asia/Shanghai'
# Broker settings

if os.getenv("SERVER_TYPE") == 'ali':
    redis_ip = '172.16.174.58'
    rabbitmq_ip = '172.16.174.58'
else:
    redis_ip = '121.43.169.83'
    rabbitmq_ip = '121.43.169.83'

if os.getenv("SERVER_TYPE") == 'ali':
    result_backend = 'redis://:epiCLOUDS2018@%s/15' % (redis_ip,)
    broker_url = 'amqp://data:epiCLOUDS2018@%s/machine_online_offline_broker' % (rabbitmq_ip,)
else:
    result_backend = 'redis://:epiCLOUDS2018@%s/15' % (redis_ip,)
    broker_url = 'amqp://data:epiCLOUDS2018@%s/machine_online_offline_broker' % (rabbitmq_ip,)

# --basic_auth = 'data:epiCLOUDS2018'
# --persistent = True  # 启用持久模式，如果打开了则Flower保存当前状态并在重新启动重载
# --port = 9090
# --logging = 'DEBUG'
