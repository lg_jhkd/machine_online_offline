import os
from datetime import timedelta
from celery.schedules import crontab

if os.getenv("SERVER_TYPE") == 'ali':
    redis_ip = '172.16.174.58'
    rabbitmq_ip = '172.16.174.58'
else:
    redis_ip = '121.43.169.83'
    rabbitmq_ip = '121.43.169.83'

if os.getenv("SERVER_TYPE") == 'ali':
    result_backend = 'redis://:epiCLOUDS2018@%s/15' % (redis_ip,)
    broker_url = 'amqp://data:epiCLOUDS2018@%s/machine_online_offline_broker' % (rabbitmq_ip,)
else:
    result_backend = 'redis://:epiCLOUDS2018@%s/15' % (redis_ip,)
    broker_url = 'amqp://data:epiCLOUDS2018@%s/machine_online_offline_broker' % (rabbitmq_ip,)



imports = ("offline_sku_in_odoo",
              "online_sku_in_odoo"
              )
# enable_utc = False
timezone = 'Asia/Shanghai'
beat_schedule = {
    'compensate': {
        'task': 'offline_sku_in_odoo.main',
        'schedule': crontab(hour=8-8, minute=00),
        # 'schedule': timedelta(seconds=2),
        'args': ()
    },
    'prepare': {
        'task': 'online_sku_in_odoo.main',
        'schedule': crontab(hour=8-8, minute=30),
        # 'schedule': timedelta(seconds=2),
        'args': ()
    }
}