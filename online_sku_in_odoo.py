from utils.log_module.get_logger import get_online_logger
from weixin_tools import WeixinTools
from celery_task import app
from config.inc import (OPERATORS,
                        PROGRAMMERS,
                        ONLINE_LIMIT)
import database_conn
import os
import traceback
import requests
import time
import datetime
import pandas


logger = get_online_logger()

class OnlineSkuInOdoo():
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(OnlineSkuInOdoo, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        if os.getenv("LOCAL"):
            self.odoo_conn = database_conn.get_odoo_conn(db="odoo_test", model="w")
            self.etl_odoo_conn = database_conn.get_etl_odoo_conn(db="etl_odoo_test")
        else:
            self.odoo_conn = database_conn.get_odoo_conn(model="w")
            self.etl_odoo_conn = database_conn.get_etl_odoo_conn()
        # self.odoo_conn = database_conn.get_odoo_conn(model="w")
        # self.etl_odoo_conn = database_conn.get_etl_odoo_conn()

        self.odoo_cursor = self.odoo_conn.cursor()
        self.etl_odoo_cursor = self.etl_odoo_conn.cursor()
        self.image_match_conn = database_conn.get_image_match_conn(model="w")
        self.image_match_cursor = self.image_match_conn.cursor()
        self.supplier_conn = database_conn.get_supplier_conn(model="r")
        self.supplier_cursor =self.supplier_conn.cursor()
        self.product_center_conn = database_conn.get_product_center_conn()
        self.product_center_cursor = self.product_center_conn.cursor()
        self.xs_idoo_conn, self.yh_idoo_conn, self.xy_idoo_conn = database_conn.get_idoo_conn()
        self.xs_idoo_cursor = self.xs_idoo_conn.cursor()
        self.yh_idoo_cursor = self.yh_idoo_conn.cursor()
        self.xy_idoo_cursor = self.xy_idoo_conn.cursor()
        self.weixin = WeixinTools()
        self.statistics_info = {}

    def __del__(self):
        self.odoo_conn.close()
        self.etl_odoo_conn.close()
        self.image_match_conn.close()
        self.supplier_conn.close()
        self.product_center_conn.close()
        self.xs_idoo_conn.close()
        self.yh_idoo_conn.close()
        self.xy_idoo_conn.close()

    def online_sku_in_odoo(self):
        """
        主函数
        :return:
        """

        fast_on_shelf_sku_list = self._load_fast_on_shelf_sku_list()
        logger.info("由于fast_on_shelf不能上架的sku数量: %d" % len(fast_on_shelf_sku_list))
        blacklist_sku_list = self._load_blacklist_sku_list()
        logger.info("由于黑名单不能上架的sku数量: %d" % len(blacklist_sku_list))
        can_not_online_sku_list = fast_on_shelf_sku_list + blacklist_sku_list
        logger.info("由于以上所有原因不能上架的sku总数: %d" % len(can_not_online_sku_list))

        to_online_sku_list_by_supplier = self._load_to_online_sku_by_supplier(can_not_online_sku_list)
        self.statistics_info['to_online_sku_by_supplier_count'] = len(to_online_sku_list_by_supplier)
        if len(to_online_sku_list_by_supplier) > ONLINE_LIMIT:
            logger.error('由于供应商问题导致的上架商品太多， 数量为: %s' % len(to_online_sku_list_by_supplier))
            self.weixin.send_message("由于供应商问题导致的上架商品数量超过%d, 数量为%d" % (ONLINE_LIMIT, len(to_online_sku_list_by_supplier)), to_users=OPERATORS)
            return
        to_online_sku_list_by_stock = self._load_to_online_sku_by_stock(can_not_online_sku_list)
        self.statistics_info['to_online_sku_by_stock_count'] = len(to_online_sku_list_by_stock)
        to_online_sku_list = to_online_sku_list_by_supplier+to_online_sku_list_by_stock
        logger.info("需要上架的sku总数: %d" % len(to_online_sku_list))
        self.statistics_info['to_online_sku_count'] = len(to_online_sku_list)


        if not to_online_sku_list:
            logger.error('没有需要上架的sku, 程序退出')
            self.weixin.send_message('online_sku_in_odoo: 没有需要上架的sku, 程序退出', OPERATORS)
            return

        try:
            to_online_product_list = self._online_sku_in_product_product(to_online_sku_list)
            onlined_product_list = self._online_product_in_product_template(to_online_product_list)#todo
            self._record_sku_by_supplier(to_online_sku_list_by_supplier)
            self._record_sku_by_stock(to_online_sku_list_by_stock)
            self._record_product(onlined_product_list, "online")
            self._record_product(list(set(to_online_product_list)-set(onlined_product_list)), "update")
            self._commit()
            self.__update_app_list(onlined_product_list, 'online')
            self.__update_app_list(list(set(to_online_product_list)-set(onlined_product_list)), 'update')
            # for product_tmpl_id in to_online_product_list:
            #     if product_tmpl_id in onlined_product_list:
            #         self._update_app(product_tmpl_id, 'online')
            #     else:
            #         self._update_app(product_tmpl_id, 'update')


            statics_str = """
                ==========机器上架_%s==========
                因为供应商算分恢复上架的商品数: %d
                因为有库存上架的商品数: %d
                上架商品总数: %d
            """ % (datetime.datetime.now(),self.statistics_info['to_online_sku_by_supplier_count'], self.statistics_info['to_online_sku_by_stock_count'], self.statistics_info['to_online_sku_count'])
            self.weixin.send_message(statics_str, to_users=OPERATORS)
        except Exception as e:
            error_msg = traceback.format_exc()
            logger.error(error_msg)
            self.weixin.send_message(error_msg, OPERATORS)
            self.odoo_conn.rollback()
            self.etl_odoo_conn.rollback()
            raise Exception

    def __update_app_list(self, product_template_ids, action):
        if not os.getenv("LOCAL"):
            ip = "http://123.207.1.32:8005/multi_mns/"
        else:
            ip = "http://123.207.1.32:8005/multi_mns/"
        try:
            form_data = {
                "product_template_id_list": product_template_ids,
                "action": action
            }
            r = requests.post(ip, json=form_data)
            # return r.json()["status"]
        except Exception as e:
            print(traceback.format_exc())
            return False

    def _record_product(self, product_list, operation):
        if len(product_list)==0:
            return
        sql = """
            select 
              id ,product_no
            from 
              product_template
            where
              id in (%s) 
        """ % ','.join(str(i) for i in product_list)
        self.odoo_cursor.execute(sql)
        product_id_no_list = self.odoo_cursor.fetchall()
        product_id_no_dict = {i[0]: i[1] for i in product_id_no_list}

        create_time = datetime.datetime.now()
        product_data_list = [(i, product_id_no_dict[i], operation, create_time) for i in product_list]
        sql = """
            insert into
              machine_online_product
            ( product_template_id,
              product_no,
              operation,
              create_time
            )
            values  
              (%s, %s, %s, %s)
        """
        self.etl_odoo_cursor.executemany(sql, product_data_list)

    def _load_to_online_sku_by_stock(self, can_not_online_sku_list):
        """
        获取由于库存恢复需要上架的sku
        :param can_not_online_sku_list:
        :return:
        """
        sql = """
            select
              sku_id, count(sku_id) as cnt
            from
              idle_sku
            where
              status != 'IDLE_TRASH'
            group by 
              sku_id
            having
              cnt>=2
        """
        self.xy_idoo_cursor.execute(sql)
        xy_idoo_res = self.xy_idoo_cursor.fetchall()
        xy_idoo_dict = {i[0]:i[1] for i in xy_idoo_res}
        self.yh_idoo_cursor.execute(sql)
        yh_idoo_res = self.yh_idoo_cursor.fetchall()
        yh_idoo_dict = {i[0]: i[1] for i in yh_idoo_res}
        self.xs_idoo_cursor.execute(sql)
        xs_idoo_res = self.xs_idoo_cursor.fetchall()
        xs_idoo_dict = {i[0]: i[1] for i in xs_idoo_res}
        logger.info("xy_idoo长度: %d, yh_idoo长度: %d, xs_idoo长度: %d" %(len(xy_idoo_res), len(yh_idoo_res), len(xs_idoo_res)))

        sku_ids_in_stock = []
        for sku_id, xy_idoo_count in xy_idoo_dict.items():
            yh_idoo_count = yh_idoo_dict.get(sku_id, 0)
            xs_idoo_count = xs_idoo_dict.get(sku_id, 0)
            all_idoo_count = xy_idoo_count + xs_idoo_count + yh_idoo_count
            if xy_idoo_count>2 and yh_idoo_count>2 and xs_idoo_count>2 and all_idoo_count > 6:
                sku_ids_in_stock.append(sku_id)
        sku_list_by_stock = list(set(sku_ids_in_stock) - set(can_not_online_sku_list))
        sql = """
            select 
              id
            from
              product_product
            where 
              id in (%s)
            and
              active=false 
        """ % ','.join(str(i) for i in sku_list_by_stock)
        self.odoo_cursor.execute(sql)
        to_online_sku_list_by_stock = self.odoo_cursor.fetchall()
        to_online_sku_list_by_stock = [i[0] for i in to_online_sku_list_by_stock]
        logger.info("因为库存原因可以上架的sku数量: %d" % len(to_online_sku_list_by_stock))
        return to_online_sku_list_by_stock

    # todo
    def _record_sku_by_supplier(self, sku_list):
        """
        记录因为供应商原因上架的sku
        :param sku_list:
        :return:
        """
        create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        sku_data_list = [(sku, 1, create_time) for sku in sku_list]
        sql = """
            insert into
              machine_online_sku
            ( sku_id,
              online_reason,
              create_time
            )
            values  
              (%s, %s, %s)
        """
        self.etl_odoo_cursor.executemany(sql, sku_data_list)

    def _record_sku_by_stock(self, sku_list):
        """
        记录由于库存原因上架的sku
        :param sku_list:
        :return:
        """
        create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        sku_data_list = [(sku, 2, create_time) for sku in sku_list]
        sql = """
            insert into
              machine_online_sku
            ( sku_id,
              online_reason,
              create_time
            )
            values  
              (%s, %s, %s)
        """
        self.etl_odoo_cursor.executemany(sql, sku_data_list)

    def _load_blacklist_sku_list(self):
        sql = """
            select 
              sku_id
            from
              sku_online_blacklist  
        """
        self.etl_odoo_cursor.execute(sql)
        blacklist_sku_list = self.etl_odoo_cursor.fetchall()
        blacklist_sku_list = [i[0] for i in blacklist_sku_list]
        return blacklist_sku_list

    def _load_fast_on_shelf_sku_list(self):
        sql = """
            select
              pp.id
            from
              product_product as pp
            join
              product_template as pt
            on
              pt.id=pp.product_tmpl_id
            where
              pt.daily_testing_flag=2
        """
        self.odoo_cursor.execute(sql)
        fast_on_shelf_sku_list = self.odoo_cursor.fetchall()
        fast_on_shelf_sku_list = [i[0] for i in fast_on_shelf_sku_list]
        return fast_on_shelf_sku_list

    def _online_sku_in_product_product(self, to_online_sku_list):
        """
        上架sku
        :param to_online_sku_list:
        :return:
        """
        to_online_sku_str = ','.join(str(i) for i in to_online_sku_list)
        sql = """
            update
              product_product
            set
              active = true,
              offline_reason = 0,
              write_date = CURRENT_TIMESTAMP 
            where
              id in (%s)
            returning
              product_tmpl_id
        """ % to_online_sku_str
        self.odoo_cursor.execute(sql)
        to_online_product_list = self.odoo_cursor.fetchall()
        to_online_product_list = list(set(to_online_product_list))
        to_online_product_list = [i[0] for i in to_online_product_list]
        logger.info('已更新sku信息，但需要更新的商品信息数量: %d' % len(to_online_product_list))
        return to_online_product_list

    def _online_product_in_product_template(self, to_online_product_list):
        """
        上架spu
        :param to_online_product_list:
        :return:
        """
        sql = """
            update
              product_template
            set
              active = true,
              website_published = true,
              sale_ok = true,
              write_date = CURRENT_TIMESTAMP 
            where 
              id in (%s)
            and 
              website_published = false
            returning
              id
        """ % ','.join(str(i) for i in to_online_product_list)
        self.odoo_cursor.execute(sql)
        onlined_product_list = self.odoo_cursor.fetchall()
        onlined_product_list = [i[0] for i in onlined_product_list]
        return onlined_product_list




    def _load_to_online_sku_by_supplier(self, can_not_online_sku_list):
        """
        获取供应商算分大于-1000000的sku
        :return:
        """
        offline_sku_list = self._load_offline_sku()
        if offline_sku_list is None:
            return
        offline_sku_str = ','.join(str(i[0]) for i in offline_sku_list)
        # can_not_online_sku_list_str = ','.join(str(i) for i in can_not_online_sku_list)
        sql = """
            select
              distinct stock_keep_unit_id
            from
              sku_link_rel
            where
              score > -1000000
            and
              stock_keep_unit_id in (%s) 
        """ % (offline_sku_str)
        self.supplier_cursor.execute(sql)
        to_online_sku_list_by_supplier = [i[0] for i in self.supplier_cursor.fetchall()]
        to_online_sku_list_by_supplier = list(set(to_online_sku_list_by_supplier)-set(can_not_online_sku_list))

        logger.info('获取由于供应商问题可以重新上架的商品, 数量: %d' % len(to_online_sku_list_by_supplier))
        return to_online_sku_list_by_supplier



    def _load_offline_sku(self):
        """
        获取由于供应商问题下架的sku(offline_reason == 1)
        :return:
        """
        sql = """
              select 
                id 
              from 
                product_product 
              where 
                active = false 
              and 
                offline_reason = 1
        """
        self.odoo_cursor.execute(sql)
        offline_sku_list = self.odoo_cursor.fetchall()
        if not offline_sku_list:
            logger.error('product_product 中没有需要上架的sku, 程序退出')
            self.weixin.send_message('online_sku_in_odoo: product_product 中没有需要上架的sku, 程序退出', OPERATORS)
            return
        logger.info('从 product_product 中获取下架的商品, 数量: %d' % len(offline_sku_list))
        return offline_sku_list

    def _commit(self):
        try:
            self.odoo_conn.commit()
            self.etl_odoo_conn.commit()
        except Exception as e:
            logger.error(traceback.format_exc())
            self.odoo_conn.rollback()
            self.etl_odoo_conn.rollback()

    def _update_app(self, product_template_id, action):
        """
        通知产品组商品状态有变化
        :param product_template_id:
        :param action:
        :return:
        """
        if not os.getenv("LOCAL"):
            ip = "http://123.207.1.32:8005/mns/"
        else:
            ip = "http://123.207.1.32:8005/mns/"
        try:
            form_data = {
                "product_template_id": product_template_id,
                "action": action
            }
            r = requests.post(ip, json=form_data)
            # return r.json()["status"]
        except Exception as e:
            print(traceback.format_exc())
            return False

@app.task
def main():
    start = time.time()
    logger.info("============start processing at %s============================" % time.strftime("%Y-%m-%d %H:%M:%S"))
    online_sku_in_odoo = OnlineSkuInOdoo()
    online_sku_in_odoo.online_sku_in_odoo()

    # online_sku_in_odoo.record_sku_by_supplier([123])

    logger.info("============end processing at %s============================" % time.strftime("%Y-%m-%d %H:%M:%S"))
    end = time.time()
    logger.info('花费时间: %s 秒', end-start)

if __name__ == '__main__':
    main()


