from config.inc import (OPERATORS,
                        PROGRAMMERS,
                        OFFLINE_LIMIT)
from utils.log_module.get_logger import get_offline_logger
from celery_task import app
from weixin_tools import WeixinTools
import database_conn
import datetime
import os
import requests
import traceback
import time
import pandas

logger = get_offline_logger()

class OfflineSkuInOdoo():
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(OfflineSkuInOdoo, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        if os.getenv("LOCAL"):
            self.odoo_conn = database_conn.get_odoo_conn(db="odoo_test", model="w")
            self.etl_odoo_conn = database_conn.get_etl_odoo_conn(db="etl_odoo_test")
        else:
            self.odoo_conn = database_conn.get_odoo_conn(model="w")
            self.etl_odoo_conn = database_conn.get_etl_odoo_conn()
        # self.odoo_conn = database_conn.get_odoo_conn(model="w")
        # self.etl_odoo_conn = database_conn.get_etl_odoo_conn()

        self.supplier_conn = database_conn.get_supplier_conn(model="r")
        self.odoo_cursor = self.odoo_conn.cursor()
        self.etl_odoo_cursor = self.etl_odoo_conn.cursor()
        self.image_match_conn = database_conn.get_image_match_conn(model="w")
        self.image_match_cursor = self.image_match_conn.cursor()
        self.supplier_cursor =self.supplier_conn.cursor()
        self.xs_idoo_conn, self.yh_idoo_conn, self.xy_idoo_conn = database_conn.get_idoo_conn()
        self.xs_idoo_cursor = self.xs_idoo_conn.cursor()
        self.yh_idoo_cursor = self.yh_idoo_conn.cursor()
        self.xy_idoo_cursor = self.xy_idoo_conn.cursor()
        self.weixin = WeixinTools()
        self.statistics_info = {}

    def __del__(self):
        self.odoo_conn.close()
        self.etl_odoo_conn.close()
        self.image_match_conn.close()
        self.supplier_conn.close()
        self.xs_idoo_conn.close()
        self.yh_idoo_conn.close()
        self.xy_idoo_conn.close()

    def _load_fast_on_shelf_sku_list(self):
        """
        获取正在测试的商品，不下架
        :return: [sku_id1, sku_id2, ...]
        """
        sql = """
            select
              pp.id
            from
              product_product as pp
            join
              product_template as pt
            on
              pt.id=pp.product_tmpl_id
            where
              pt.daily_testing_flag=1
        """
        self.odoo_cursor.execute(sql)
        fast_on_shelf_sku_list = self.odoo_cursor.fetchall()
        fast_on_shelf_sku_list = [i[0] for i in fast_on_shelf_sku_list]
        return fast_on_shelf_sku_list

    def _load_blacklist_sku_list(self):
        """
        获取在下架黑名单里的商品，不下架
        :return: [sku_id1, sku_id2, ...]
        """
        sql = """
            select 
              sku_id
            from
              sku_offline_blacklist  
        """
        self.etl_odoo_cursor.execute(sql)
        blacklist_sku_list = self.etl_odoo_cursor.fetchall()
        blacklist_sku_list = [i[0] for i in blacklist_sku_list]
        return blacklist_sku_list

    def offline_sku_in_odoo(self):
        """
        下架主程序
        :return:
        """

        # 获取由于各种原因不能下架的商品
        in_stock_sku_list = self._load_sku_list_in_stock()
        logger.info("由于供应商还有库存不能下架的sku数量: %d" % len(in_stock_sku_list))
        in_self_stcok_sku_list = self._load_sku_list_in_self_stock()
        logger.info("由于自己还有库存不能下架的sku数量: %d" % len(in_self_stcok_sku_list))
        fast_on_shelf_sku_list = self._load_fast_on_shelf_sku_list()
        logger.info("由于fast_on_shelf不能下架的sku数量: %d" % len(fast_on_shelf_sku_list))
        blacklist_sku_list = self._load_blacklist_sku_list()
        logger.info("由于黑名单不能下架的sku数量: %d" % len(blacklist_sku_list))
        can_not_offline_sku_list = in_stock_sku_list+in_self_stcok_sku_list+fast_on_shelf_sku_list+blacklist_sku_list
        logger.info("由于以上全部原因不能下架的sku总数: %d" % len(can_not_offline_sku_list))

        # 获取需要下架的商品
        # to_offline_sku_list_by_supplier = []
        to_offline_sku_list_by_supplier = self._load_to_offline_sku_by_supplier(can_not_offline_sku_list)
        logger.info("由于供应商问题需要下架的sku数量为: %d" % len(to_offline_sku_list_by_supplier))
        self.statistics_info['to_offline_sku_by_supplier_count'] = len(to_offline_sku_list_by_supplier)
        to_offline_sku_list_by_merge, to_offline_cluster_id_dict = self._load_to_offline_sku_by_merge(can_not_offline_sku_list)
        logger.info("由于合并问题需要下架的sku数量为: %d" % len(to_offline_sku_list_by_merge))
        self.statistics_info['to_offline_sku_by_merge_count'] = len(to_offline_sku_list_by_merge)
        self.statistics_info['to_offline_sku_count'] = self.statistics_info['to_offline_sku_by_supplier_count']+self.statistics_info['to_offline_sku_by_merge_count']

        to_offline_sku_list_by_merge2 = [i[0] for i in to_offline_sku_list_by_merge]
        to_offline_sku_list = []
        to_offline_sku_list += to_offline_sku_list_by_merge2
        to_offline_sku_list += to_offline_sku_list_by_supplier
        logger.info("需要下架的sku总数: %d" % (len(to_offline_sku_list)))


        if len(to_offline_sku_list) > OFFLINE_LIMIT:
            logger.error("需要下架的sku总数超过%d，程序退出" % OFFLINE_LIMIT)
            self.weixin.send_message("需要下架的sku总数超过%d, 数量为%d" % (OFFLINE_LIMIT, len(to_offline_sku_list)), to_users=OPERATORS)
            return

        # 更新数据库
        try:
            to_update_product_list, to_offline_product_list = self._offline_sku_and_product(to_offline_sku_list_by_supplier, to_offline_sku_list_by_merge2)
            self._record_sku_by_supplier(to_offline_sku_list_by_supplier)
            self._record_sku_by_merge(to_offline_sku_list_by_merge, to_offline_cluster_id_dict)
            self._record_product(to_offline_product_list, "offline")
            self._record_product(list(set(to_update_product_list)-set(to_offline_product_list)), "update")
            self._commit()
            self.__update_app_list(to_offline_product_list, 'offline')
            self.__update_app_list(list(set(to_update_product_list) - set(to_offline_product_list)), 'update')
            # for product_tmpl_id in to_update_product_list:
            #     if product_tmpl_id in to_offline_product_list:
            #         self._update_app(product_tmpl_id, "offline")
            #     else:
            #         self._update_app(product_tmpl_id, "update")
            statistics_str = """
            ==========机器下架_%s==========
                在架商品数: %d
                因为供应商问题下架的商品数: %d
                因为合并问题下架的商品数: %d
                下架商品总数: %d
            """ % (datetime.datetime.now(), self.statistics_info['online_cluster_id_count'], self.statistics_info['to_offline_sku_by_supplier_count'], self.statistics_info['to_offline_sku_by_merge_count'], self.statistics_info['to_offline_sku_count'])
            self.weixin.send_message(statistics_str, to_users=OPERATORS)
        except Exception as e:
            error_msg = traceback.format_exc()
            logger.error(error_msg)
            self.weixin.send_message(error_msg, OPERATORS)
            self.odoo_conn.rollback()
            self.etl_odoo_conn.rollback()
            raise Exception

    def __update_app_list(self, product_template_ids, action):
        if not os.getenv("LOCAL"):
            ip = "http://123.207.1.32:8005/multi_mns/"
        else:
            ip = "http://123.207.1.32:8005/multi_mns/"
        try:
            form_data = {
                "product_template_id_list": product_template_ids,
                "action": action
            }
            r = requests.post(ip, json=form_data)
            # return r.json()["status"]
        except Exception as e:
            print(traceback.format_exc())
            return False

    def _record_product(self, product_list, operation):
        if len(product_list)==0:
            return
        sql = """
            select 
              id ,product_no
            from 
              product_template
            where
              id in (%s) 
        """ % ','.join(str(i) for i in product_list)
        self.odoo_cursor.execute(sql)
        product_id_no_list = self.odoo_cursor.fetchall()
        product_id_no_dict = {i[0]:i[1] for i in product_id_no_list}

        create_time = datetime.datetime.now()
        product_data_list = [(i, product_id_no_dict[i], operation, create_time) for i in product_list]
        sql = """
            insert into
              machine_offline_product
            ( product_template_id,
              product_no,
              operation,
              create_time
            )
            values  
              (%s, %s, %s, %s)
        """
        self.etl_odoo_cursor.executemany(sql, product_data_list)



    def _get_to_offline_product_ids(self, df_orign):
        """
        从df中选出所有active=False的product_tmpl_id
        :param df_orign: 三列: product_tmpl_id, id(sku_id), active
        :return:
        """
        df_orign_group = df_orign.groupby('product_tmpl_id')
        df_false = df_orign[df_orign['active'] == False]
        df_false_group = df_false.groupby('product_tmpl_id')
        df_orign_group_count_df = pandas.DataFrame(df_orign_group.size().reset_index(name="all_count"))
        df_false_group_count_df = pandas.DataFrame(df_false_group.size().reset_index(name="false_count"))
        df_result = df_false_group_count_df.merge(df_orign_group_count_df, on="product_tmpl_id", how="outer")
        to_offline_product_df = df_result[df_result['false_count'] == df_result['all_count']]
        to_offline_product_list = list(to_offline_product_df['product_tmpl_id'])
        return [int(i) for i in to_offline_product_list]

    def _update_sku_in_product_product(self, to_offline_sku_list_by_supplier, to_offline_sku_list_by_merge):
        """
        更新product_product表中的sku，将其下架
        :param to_offline_sku_list_by_supplier:
        :param to_offline_sku_list_by_merge:
        :return:
        """
        sql = """
            update 
              product_product 
            set 
              active = false, 
              offline_reason=1, 
              write_date = CURRENT_TIMESTAMP 
            where 
              id in (%s)
            and 
              active=true 
            and 
              offline_reason=0 
        """ % ','.join(str(i) for i in to_offline_sku_list_by_supplier)
        if len(to_offline_sku_list_by_supplier)>0:
            self.odoo_cursor.execute(sql)
        logger.info('已下架供应商原因sku, 数量为: %d' % len(to_offline_sku_list_by_supplier))
        sql = """
                update 
                  product_product 
                set 
                  active = false, 
                  offline_reason=5, 
                  write_date = CURRENT_TIMESTAMP 
                where 
                  id in (%s)
                and 
                  active=true 
                and 
                  offline_reason=0 
            """ % ','.join(str(i) for i in to_offline_sku_list_by_merge)
        if len(to_offline_sku_list_by_merge) > 0:
            self.odoo_cursor.execute(sql)
        logger.info('已下架合并原因sku, 数量为: %d' % len(to_offline_sku_list_by_merge))

    def _update_product_in_product_template(self, to_offline_product_list):
        """
        下架商品
        :param to_offline_product_list: 全部sku都下架的商品
        :param to_update_product_list: 所有需要更新状态的商品，包括了to_offline_product_list
        :return:
        """
        sql = """
            update
              product_template
            set
              active=false,
              website_published=false,
              sale_ok=false,
              write_date=CURRENT_TIMESTAMP
            where
              id in (%s)
        """ % ','.join(str(i) for i in to_offline_product_list)
        self.odoo_cursor.execute(sql)
        logger.info("已下架商品, 数量为: %d" % len(to_offline_product_list))


    def _offline_sku_and_product(self, to_offline_sku_list_by_supplier, to_offline_sku_list_by_merge):
        to_offline_sku_list = list(set(to_offline_sku_list_by_supplier+to_offline_sku_list_by_merge))
        sql = """
            select
              product_tmpl_id, id, active
            from
              product_product
            where 
              product_tmpl_id
                in (select
                      distinct product_tmpl_id
                    from
                      product_product
                    where
                      id in (%s)
                    and 
                      active=true 
                    and 
                      offline_reason=0
                    )
        """ % ','.join(str(i) for i in to_offline_sku_list)
        pt_df = pandas.read_sql(sql, self.odoo_conn)
        to_update_product_list = list(set(pt_df['product_tmpl_id']))

        # 更新df里面的active
        to_update_df = pt_df[pt_df['id'].isin(to_offline_sku_list)]
        to_update_df['active']=False
        pt_df.update(to_update_df)

        # 获取所有sku的active都为false的商品
        to_offline_product_list = self._get_to_offline_product_ids(pt_df)
        logger.info('由于全部sku下架需要下架的商品数量: %d' % len(to_offline_product_list))
        self._update_sku_in_product_product(to_offline_sku_list_by_supplier, to_offline_sku_list_by_merge)
        self._update_product_in_product_template(to_offline_product_list)
        return to_update_product_list, to_offline_product_list



    def _record_sku_by_supplier(self, sku_list):
        """
        记录供应商问题机器下架商品
        :param sku_list:
        :return:
        """
        create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        sku_data_list = [(sku, 1, create_time) for sku in sku_list]
        sql = """
            insert into
              machine_offline_sku
            ( sku_id,
              offline_reason,
              create_time
            )
            values  
              (%s, %s, %s)
        """
        self.etl_odoo_cursor.executemany(sql, sku_data_list)

        # sql = """
        #     select
        #
        # """

    def _record_sku_by_merge(self, sku_list, old_and_new_cluster_id_dict):
        """
        记录合并问题机器下架商品
        :param sku_list:
        :param old_and_new_cluster_id_dict:
        :return:
        """
        create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        sku_data_list = [(str(sku[0]), 5, create_time, str(sku[1]), old_and_new_cluster_id_dict[sku[1]]) for sku in sku_list]
        # print (type(sku_data_list[0][0]))
        sql = """
            insert into
              machine_offline_sku
            ( sku_id,
              offline_reason,
              create_time,
              old_cluster_id,
              new_cluster_id
            )
            values  
              (%s, %s, %s, %s, %s)
        """
        self.etl_odoo_cursor.executemany(sql, sku_data_list)

    def _commit(self):
        try:
            self.odoo_conn.commit()
            self.etl_odoo_conn.commit()
        except Exception as e:
            logger.error(traceback.format_exc())
            self.odoo_conn.rollback()
            self.etl_odoo_conn.rollback()

    def _update_app(self, product_template_id, action):
        """
        通知产品组商品状态有变化
        :param product_template_id:
        :param action:
        :return:
        """
        if not os.getenv("LOCAL"):
            ip = "http://123.207.1.32:8005/mns/"
        else:
            ip = "http://123.207.1.32:8005/mns/"
        try:
            form_data = {
                "product_template_id": product_template_id,
                "action": action
            }
            r = requests.post(ip, json=form_data)
            # return r.json()["status"]
        except Exception as e:
            print(traceback.format_exc())
            return False


    def _load_sku_list_in_stock(self):
        """
        查看供应商库存里是否还有货
        :return:
        """
        sql = """
            select
              sku_id, count(sku_id) as cnt
            from
              idle_sku
            where
              status != 'IDLE_TRASH'
            group by 
              sku_id
            having
              cnt>5
        """
        self.xy_idoo_cursor.execute(sql)
        xy_idoo_res = self.xy_idoo_cursor.fetchall()
        xy_idoo_dict = {i[0]:i[1] for i in xy_idoo_res}
        self.yh_idoo_cursor.execute(sql)
        yh_idoo_res = self.yh_idoo_cursor.fetchall()
        yh_idoo_dict = {i[0]: i[1] for i in yh_idoo_res}
        self.xs_idoo_cursor.execute(sql)
        xs_idoo_res = self.xs_idoo_cursor.fetchall()
        xs_idoo_dict = {i[0]: i[1] for i in xs_idoo_res}
        logger.info("xy_idoo长度: %d, yh_idoo长度: %d, xs_idoo长度: %d" %(len(xy_idoo_res), len(yh_idoo_res), len(xs_idoo_res)))

        sku_ids_in_stock = []
        for sku_id, xy_idoo_count in xy_idoo_dict.items():
            yh_idoo_count = yh_idoo_dict.get(sku_id, 0)
            xs_idoo_count = xs_idoo_dict.get(sku_id, 0)
            all_idoo_count = xy_idoo_count + xs_idoo_count + yh_idoo_count
            if xy_idoo_count>5 and yh_idoo_count>5 and xs_idoo_count>5 and all_idoo_count > 20:
                sku_ids_in_stock.append(sku_id)
        logger.info("还有库存的sku数量: %d" % len(sku_ids_in_stock))
        return sku_ids_in_stock

    def _load_sku_list_in_self_stock(self):
        """
        查看自己库存是否还有货
        :return:
        """
        try:
            date_today = datetime.datetime.now().strftime("%Y%m%d")
            url = "http://stock.yuceyi.com:5678/wms/server/all_shelved_sku?except_date={}".format(date_today)
            authorization = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ilx1NWYwMFx1NTNkMVx1ODAwNSI" \
                            "sImlzX2Nvb3BlcmF0aXZlX3N1cHBsaWVyIjp0cnVlLCJsb2dpbl9uYW1lIjoiZGV2ZWxvcGVyIiwiZXhwIjoxNTE2Njc" \
                            "2NDU2LCJ1c2VyX2lkIjoxLCJ1c2VyX3V1aWQiOiJTVVA2NzQ3NTk4NTciLCJlbWFpbCI6IiJ9.amMScJFJpxGL0D1H2K" \
                            "J1EuLh5h7Sc9yTaxrn_WQiZEk"
            headers = {
                "Authorization": authorization
            }
            res = requests.get(url, headers=headers)
            sku_id_dict = res.json()
            sku_ids = [int(sku_id_str) for sku_id_str in sku_id_dict.keys()]
        except Exception as e:
            sku_ids = []
            logger.error(traceback.format_exc())
            logger.error("获取在自己库存中的sku时发生错误")
        return sku_ids

    def _load_online_cluster_id_list(self):
        """
        获取线上的所有cluster
        :return:
        """
        sql = """
            select 
              distinct cluster_id
            from
              product_template
            where
              website_published = TRUE
            and 
              active = TRUE
            and 
              sale_ok = TRUE
            and 
              cluster_id is not null
        """
        self.odoo_cursor.execute(sql)
        online_cluster_id_list = self.odoo_cursor.fetchall()
        online_cluster_id_list = [i[0] for i in online_cluster_id_list]
        return online_cluster_id_list


    def _load_merged_cluster_id_list(self, online_cluster_id_list):
        """
        获取需要合并的cluster_id
        :param online_cluster_id_list:
        :return:
        """
        sql = """
            select 
              distinct cluster_id 
            from 
              2c_item_details_c_nodes
            where
              cluster_id
            in (%s)
        """ % ','.join(str(i) for i in online_cluster_id_list)
        self.image_match_cursor.execute(sql)
        tmp_cluster_id_list = self.image_match_cursor.fetchall()

        sql = """
            select 
              distinct cluster_id
            from
              product_template
            where
              website_published = TRUE
            and 
              active = TRUE
            and 
              sale_ok = TRUE
        """
        self.odoo_cursor.execute(sql)
        merged_cluster_id_list = list(set(self.odoo_cursor.fetchall())-set(tmp_cluster_id_list))
        merged_cluster_id_list = [i[0] for i in merged_cluster_id_list]
        return merged_cluster_id_list


    def _load_to_offline_cluster_id_list(self, online_cluster_id_list, merged_cluster_id_list):
        """

        :param online_cluster_id_list:
        :param merged_cluster_id_list:
        :return: {old_cluster_id: new_cluster_id}
        """
        to_offline_cluster_id_by_merge_dict = {}
        # sql有长度限制
        per_num = 20000
        new_cluster_id_list = []
        times = int(len(merged_cluster_id_list)/per_num+1)
        for i in range(times):
            sql = """
                select
                  cluster_id, seq_num
                from
                  cluster_nodes
                where 
                  seq_num in (%s)
            """ % ','.join(str(i) for i in merged_cluster_id_list[i*per_num:(i+1)*per_num] if i is not None)
            self.image_match_cursor.execute(sql)
            new_cluster_id_list.extend(self.image_match_cursor.fetchall())

        for cluster_id in new_cluster_id_list:
            if cluster_id[0] in online_cluster_id_list:
                to_offline_cluster_id_by_merge_dict[cluster_id[1]] = cluster_id[0]

        return to_offline_cluster_id_by_merge_dict

    def _load_to_offline_sku_list_by_merge(self, to_offline_cluster_id_dict, can_not_offline_sku_list):
        """

        :param to_offline_cluster_id_dict:
        :param can_not_offline_sku_list:
        :return: [(sku_id, cluster_id), ]
        """

        sql = """
                    select
                      pp.id as sku_id, pt.cluster_id
                    from
                      product_template as pt
                    join 
                      product_product as pp
                    on 
                      pp.product_tmpl_id = pt.id
                    where
                      pt.cluster_id is not null 
                    and
                      pp.active = TRUE 
                    and
                      pt.cluster_id in (%s)
                """ % (','.join(str(i) for i in list(to_offline_cluster_id_dict)))
        sku_by_merge_df = pandas.read_sql(sql, self.odoo_conn)
        to_offline_sku_by_merge_df = sku_by_merge_df[~sku_by_merge_df['sku_id'].isin(can_not_offline_sku_list)]
        to_offline_sku_list = [tuple(x) for x in to_offline_sku_by_merge_df.values]
        return to_offline_sku_list


        # sql = """
        #     select
        #       pp.id as sku_id, pt.cluster_id
        #     from
        #       product_template as pt
        #     join
        #       product_product as pp
        #     on
        #       pp.product_tmpl_id = pt.id
        #     where
        #       pt.cluster_id is not null
        #     and
        #       pp.active = TRUE
        #     and
        #       pt.cluster_id in (%s)
        #     and
        #       pp.id not in (%s)
        # """ % (','.join(str(i) for i in list(to_offline_cluster_id_dict)), ','.join(str(i) for i in can_not_offline_sku_list))
        #
        # self.odoo_cursor.execute(sql)
        # to_offline_sku_list = self.odoo_cursor.fetchall()
        # return to_offline_sku_list

    def _load_to_offline_sku_by_merge(self, can_not_offline_sku_list):
        online_cluster_id_list = self._load_online_cluster_id_list()
        logger.info("所有线上商品的cluster_id数量为: %d" % len(online_cluster_id_list))
        self.statistics_info['online_cluster_id_count'] = len(online_cluster_id_list)
        merged_cluster_id_list = self._load_merged_cluster_id_list(online_cluster_id_list)
        logger.info("需要merged的cluster_id数量为: %d" % len(merged_cluster_id_list))

        # {old_cluster_id: new_cluster_id, ...}
        to_offline_cluster_id_dict = self._load_to_offline_cluster_id_list(online_cluster_id_list, merged_cluster_id_list)
        logger.info("由于merge需要下架的cluster_id数量为: %d" % len(to_offline_cluster_id_dict))
        if (len(to_offline_cluster_id_dict)==0):
            logger.info("由于merge需要下架的sku数量为: 0")
            return [], []

        # [(sku_id, cluster_id),(),...]
        to_offline_sku_list_by_merge = self._load_to_offline_sku_list_by_merge(to_offline_cluster_id_dict, can_not_offline_sku_list)
        logger.info("由于merge需要下架的sku数量为: %d" % len(to_offline_sku_list_by_merge))
        return to_offline_sku_list_by_merge, to_offline_cluster_id_dict



    def _load_to_offline_sku_by_supplier(self, can_not_offline_sku_list):
        online_sku_df = self._load_online_sku()
        logger.info("过滤后的上架sku数量为: %d" % len(online_sku_df))
        sql = """
                select
                  stock_keep_unit_id as sku_id
                from
                  sku_link_rel
                group by 
                  stock_keep_unit_id
                having 
                  max(score)<=-1000000     
            """
        sku_by_supplier_df = pandas.read_sql(sql, self.supplier_conn)
        online_sku_list_by_supplier = pandas.merge(sku_by_supplier_df, online_sku_df, how='inner', on=['sku_id'])
        to_offline_sku_list_by_supplier = list(online_sku_list_by_supplier[~online_sku_list_by_supplier['sku_id'].isin(can_not_offline_sku_list)]['sku_id'])
        return to_offline_sku_list_by_supplier


    def _load_online_sku(self):
        # 从odoo里面获取上架的cluster id和sku id
        sql = """
            select 
              pp.id as sku_id, cluster_id
            from 
              product_product as pp
            join 
              product_template as pt 
            on 
              pp.product_tmpl_id = pt.id
            where 
              pp.active = true 
            and 
              pp.offline_reason = 0 
            and 
              pt.pre_pro_tmpl_id is null 
            and 
              pt.cluster_id is not null
        """
        df = pandas.read_sql(sql, self.odoo_conn)
        cluster_id_df = df['cluster_id'].unique()

        sql = """
            select
              distinct cluster_id
            from
              2c_item_details_c_nodes
            where
              cluster_id
              in (%s)
        """ % ','.join(str(i) for i in cluster_id_df)
        df_2c = pandas.read_sql(sql, self.image_match_conn)
        exists_online_sku_df = pandas.merge(df, df_2c, how='inner', on=['cluster_id'])
        return exists_online_sku_df
        # exists_online_sku_list = list(pandas.merge(df, df_2c, how='inner', on=['cluster_id'])['sku_id'])
        # return exists_online_sku_list





@app.task
def main():
    start = time.time()
    logger.info("============start processing at %s============================" % time.strftime("%Y-%m-%d %H:%M:%S"))
    offline_sku_in_odoo = OfflineSkuInOdoo()
    offline_sku_in_odoo.offline_sku_in_odoo()
    # offline_sku_in_odoo.load_online_sku()
    logger.info("============end processing at %s============================" % time.strftime("%Y-%m-%d %H:%M:%S"))
    end = time.time()
    logger.info('花费时间: %s 秒', end-start)

if __name__ == '__main__':
    main()
