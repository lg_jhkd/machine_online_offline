from celery import Celery

from utils.log_module import get_logger

app = Celery('online_and_offline')
app.config_from_object('config.celery_config')


@get_logger.DecoratorLogger
def main():
    app.start()


if __name__ == '__main__':
    main()
