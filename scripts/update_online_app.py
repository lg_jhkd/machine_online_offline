import database_conn
import requests
import traceback
import os


def _update_app(product_template_id, action):
    """
    通知产品组商品状态有变化
    :param product_template_id:
    :param action:
    :return:
    """
    if not os.getenv("LOCAL"):
        ip = "http://123.207.1.32:8005/mns/"
    else:
        ip = "http://123.207.1.32:8005/mns/"
    try:
        form_data = {
            "product_template_id": product_template_id,
            "action": action
        }
        r = requests.post(ip, json=form_data)
        # return r.json()["status"]
    except Exception as e:
        print(traceback.format_exc())
        return False

etl_odoo_conn = database_conn.get_etl_odoo_conn()
etl_odoo_cursor = etl_odoo_conn.cursor()
sql = """
    select product_template_id, operation
    from machine_online_product
    where create_time='08:33:03'
"""
etl_odoo_cursor.execute(sql)
products = etl_odoo_cursor.fetchall()
for i in products:
    _update_app(i[0],i[1])
    print (i[0], i[1])